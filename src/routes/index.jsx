import { Switch, Route } from "react-router";
import { Home } from "../pages/Home";
import Signup from "../pages/Signup";

export const Routes = () => {
  return (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route exact path="/signup">
        <Signup />
      </Route>
    </Switch>
  );
};
